import { Component, Input } from '@angular/core';
import { PostClass } from './post-class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Application blog';
  posts: Array<PostClass> = new Array();

  // posts: Array<any> = new Array();
  constructor() {
    _initPosts(this.posts);
  }

}

// -- Attributs privés -- //
var latinText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt";
latinText += "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ...";
// -- Fin des attributs privés -- //

// -- Méthodes privées et inaccessibles via le DOM -- //
var _initPosts = (posts: Array<any>): void => {
  posts.push(new PostClass("Premier post", latinText, -10));
  posts.push(new PostClass("Deuxième post", latinText, 0));
  posts.push(new PostClass("Troisième post", latinText, 2));
  posts.push(new PostClass("Quatrième post", latinText, 2));
  console.log(posts);
}
// -- Fin des méthodes privées -- //