import { Component, OnInit, Input } from '@angular/core';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { PostClass } from '../post-class';

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})

export class PostListComponent implements OnInit {

  // post venant du component parent
  @Input() post: PostClass;

  faThumbsUp = faThumbsUp;
  faThumbsDown =  faThumbsDown;

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * @description ajoute ou supprime un 'like' au post concerné
   * @param {boolean} addLike
   */
  addLike(addLike: boolean) {
    this.post.loveIts = addLike ? this.post.loveIts + 1 : this.post.loveIts - 1;
    console.log(this.post.loveIts);
  }

  /**
   * @description permet de savoir si un post est populaire ou non
   */
  isPopular():boolean {
    if (this.post.loveIts > 0) {
        return true;
    } else if (this.post.loveIts < 0) {
        return false;
    } else {
      return null;
    }
  }

}
